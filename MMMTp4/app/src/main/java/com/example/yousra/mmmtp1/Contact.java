package com.example.yousra.mmmtp1;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yousra on 20/01/17.
 */




    /**
     * Created by yousra on 15/01/17.
     */

    public class Contact  {
        private long id;
        private String name;
        private String prenom;
        private String dateNaissance;
        private String ville;


     public Contact(){

     }
        public long getId(){
            return id;
        }

        public void setId(long id){
            this.id=id;
        }
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setPrenom(String prenom) {
            this.prenom = prenom;
        }

        public void setDateNaissance(String dateNaissance) {
            this.dateNaissance = dateNaissance;
        }

        public void setVille(String ville) {
            this.ville = ville;
        }

        public String getPrenom() {

            return prenom;
        }

        public String getDateNaissance() {
            return dateNaissance;
        }

        public String getVille() {
            return ville;
        }


        public Contact(String nom, String prenom, String date, String ville) {
            this.name = nom;
            this.prenom=prenom;
            this.dateNaissance=date;
            this.ville=ville;
        }

        @Exclude
        public Map<String, Object> toMap() {
            HashMap<String, Object> result = new HashMap<>();
            result.put("name", name);
            result.put("prenom", prenom);
            result.put("dateNaissance", dateNaissance);
            result.put("ville", ville);
            return result;
        }

    }


