package com.example.yousra.mmmtp1;

import android.app.LauncherActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Activity1 extends AppCompatActivity {
    private final String TAG = "Activity1";
    DatabaseReference mDB;
    DatabaseReference mListItemRef;
    private RecyclerView mListItemsRecyclerView;
    private ListItemsAdapter mlistAdapter;
   // private HashMap<String, String> map2;
    private ArrayList<Contact> myListItems;
    LibraryContentProvider contact= new LibraryContentProvider(this);

    static final  int NEW_CLIENT=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        EditText filter = (EditText) findViewById(R.id.filter);
        mDB= FirebaseDatabase.getInstance().getReference();
        mListItemRef = mDB.child("listItem");
        myListItems = new ArrayList<>();

        mListItemsRecyclerView = (RecyclerView)findViewById(R.id.listItem_recycler_view);
        //mListItemsRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        mListItemsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        updateUI();

        Button valider = (Button) findViewById(R.id.button2);
        valider.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Activity1.this, MainActivity.class);
                startActivityForResult(intent, NEW_CLIENT);
            }
        });

        //Création de la ArrayList qui nous permettra de remplire la listView
//        mapItems = new ArrayList<>();
//        map = new HashMap<String, String>();
//        map.put("name", "elghzizal  ");
//        map.put("prenom", "yousra  ");
//        map.put("dateNaissance", "1994  ");
//        map.put("ville", "fez");
//
////enfin on ajoute cette hashMap dans la arrayList
//        mapItems.add(map);

//        List<Contact> liste = new ArrayList<>();
//        contact.open();
//        liste = contact.getAllContacts();
//        for (int i = 0; i < liste.size(); i++) {
//            HashMap<String, String> map2=new HashMap<String, String>();
//            map2.put("name", liste.get(i).getName()+"  ");
//         map2.put("prenom", liste.get(i).getPrenom()+"  ");
//            map2.put("dateNaissance", liste.get(i).getDateNaissance()+"  ");
//            map2.put("ville", liste.get(i).getVille());
//            mapItems.add(map2);
//        }
//        contact.close();

        mListItemRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG+"Added",dataSnapshot.getValue(Contact.class).toString());
                fetchData(dataSnapshot);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG+"Changed",dataSnapshot.getValue(Contact.class).toString());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG+"Removed",dataSnapshot.getValue(Contact.class).toString());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG+"Moved",dataSnapshot.getValue(Contact.class).toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG+"Cancelled",databaseError.toString());
            }
        });
    }

//            // Création d'un SimpleAdapter qui se chargera de mettre les items présent dans notre list (mapItems) dans la vue item.xml
//            mlistAdapter = new SimpleAdapter(this.getBaseContext(), mapItems, R.layout.item,
//                    new String[]{"name", "prenom", "dateNaissance", "ville"}, new int[]{R.id.textView1, R.id.textView2, R.id.textView3, R.id.textView4});
//            //ici on affecte l'adapteur pour la listView afin de la remplir avec les elemets de item
//            malistView.setAdapter(mlistAdapter);
//        //Fonction Pour la recherche par nom :
//       TextWatcher inputfilter = new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//             //  contact.open();
//               //for(int i=0;i< contact.getAllContacts().size();i++) {
//                 //  s = contact.getAllContacts().get(i).getName();
//                   mlistAdapter.getFilter().filter(s); //TODO:Stephen§
//               //}
//                //contact.close();
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//
//
//        };
//
//        filter.addTextChangedListener(inputfilter );
//
//    }


    public void onActivityResult(int requestcode,int resultcode,Intent data){
        if(requestcode==NEW_CLIENT){
            if(resultcode==RESULT_OK) {
//                map = new HashMap<String, String>();
//                map.put("name", data.getStringExtra("nom"));
//                map.put("prenom", data.getStringExtra("prenom"));
//                map.put("dateNaissance", data.getStringExtra("dateNaissance"));
//                map.put("ville ", data.getStringExtra("ville"));
//                myListItems.add(map);
//                mlistAdapter.notifyDataSetChanged();

                    // Create new List Item  at /listItem
                    final String key = FirebaseDatabase.getInstance().getReference().child("listItem").push().getKey();
                    LayoutInflater li = LayoutInflater.from(this);
                    View getListItemView = li.inflate(R.layout.item, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setView(getListItemView);

                final Intent DATA = data;
                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,int id) {
                                    // get user input and set it to result
                                    // edit text
                                   String name = DATA.getStringExtra("nom");
                                    String prenom = DATA.getStringExtra("prenom");
                                    String dateNaissance = DATA.getStringExtra("dateNaissance");
                                    String ville = DATA.getStringExtra("ville");
                                    Contact contact = new Contact(name,prenom,dateNaissance,ville);
                                    Map<String, Object> listItemValues = contact.toMap();
                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put("/listItem/" + key, listItemValues);
                                    FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates);

                                }
                            }).create()
                            .show();


            }
        }
    }

    private void fetchData(DataSnapshot dataSnapshot) {
        Contact listItem=dataSnapshot.getValue(Contact.class);
        myListItems.add(listItem);
        updateUI();
    }

    private void updateUI(){
        mlistAdapter = new ListItemsAdapter(myListItems);
        mListItemsRecyclerView.setAdapter(mlistAdapter);
    }

    private class ListItemsHolder extends RecyclerView.ViewHolder{
        public TextView mNameTextView1;
        public TextView mNameTextView2;
        public TextView mNameTextView3;
        public TextView mNameTextView4;
        public ListItemsHolder(View itemView){
            super(itemView);
            mNameTextView1 = (TextView) itemView.findViewById(R.id.textView1);
            mNameTextView2 = (TextView) itemView.findViewById(R.id.textView2);
            mNameTextView3 = (TextView) itemView.findViewById(R.id.textView3);
            mNameTextView4 = (TextView) itemView.findViewById(R.id.textView4);
        }

        public void bindData(Contact s){
            mNameTextView1.setText(s.getName());
            mNameTextView2.setText(s.getPrenom());
            mNameTextView3.setText(s.getVille());
            mNameTextView4.setText(s.getName());
        }

    }

    private class ListItemsAdapter extends RecyclerView.Adapter<ListItemsHolder>{
        private ArrayList<Contact> mListItems;
        public ListItemsAdapter(ArrayList<Contact> ListItems){
            mListItems = ListItems;
        }
        @Override
        public ListItemsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(Activity1.this);
            View view = layoutInflater.inflate(R.layout.item,parent,false);
            return new ListItemsHolder(view);

        }
        @Override
        public void onBindViewHolder(ListItemsHolder holder, int position) {
            Contact s = mListItems.get(position);
            holder.bindData(s);
        }
        @Override
        public int getItemCount() {
            return mListItems.size();
        }
    }
}

