package com.example.yousra.mmmtp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Intent intent = getIntent();
        String nom = intent.getStringExtra("nom");
        String prenom = intent.getStringExtra("prenom");
        String date = intent.getStringExtra("date de naissance");
        String ville = intent.getStringExtra("ville");
        ((TextView)findViewById(R.id.textView)).setText("nom : "+nom, TextView.BufferType.EDITABLE);
        ((TextView)findViewById(R.id.textView2)).setText("prénom : "+prenom, TextView.BufferType.EDITABLE);
        ((TextView)findViewById(R.id.textView3)).setText("date de naissance : "+date, TextView.BufferType.EDITABLE);
        ((TextView)findViewById(R.id.textView4)).setText("ville de naissane : "+ville, TextView.BufferType.EDITABLE);

        RecuperationData recup = getIntent().getExtras().getParcelable("recuperationData");
        ((TextView)findViewById(R.id.textView5)).setText("nom : "+recup.getName(), TextView.BufferType.EDITABLE);
        ((TextView)findViewById(R.id.textView6)).setText("prénom : "+recup.getPrenom(), TextView.BufferType.EDITABLE);
        ((TextView)findViewById(R.id.textView7)).setText("date de naissance : "+recup.getDateNaissance(), TextView.BufferType.EDITABLE);
        ((TextView)findViewById(R.id.textView8)).setText("ville de naissane : "+recup.getVille(), TextView.BufferType.EDITABLE);
    }
}
