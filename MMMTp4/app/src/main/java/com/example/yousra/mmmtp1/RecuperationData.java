package com.example.yousra.mmmtp1;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yousra on 15/01/17.
 */

public class RecuperationData implements Parcelable {
    public String getName() {
        return name;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public String getVille() {
        return ville;
    }

    private String name;
        private String prenom;
        private String dateNaissance;
        private String ville;


    protected RecuperationData(Parcel in) {
        name=in.readString();
        prenom=in.readString();
        dateNaissance=in.readString();
        ville=in.readString();
    }

    public static final Creator<RecuperationData> CREATOR = new Creator<RecuperationData>() {
        @Override
        public RecuperationData createFromParcel(Parcel in) {
            return new RecuperationData(in);
        }

        @Override
        public RecuperationData[] newArray(int size) {
            return new RecuperationData[size];
        }
    };

    public RecuperationData(String nom, String prenom, String date, String ville) {
        this.name = nom;
        this.prenom=prenom;
        this.dateNaissance=date;
        this.ville=ville;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(prenom);
        dest.writeString(dateNaissance);
        dest.writeString(ville);
    }
}
