package com.example.yousra.mmmtp1;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button valider=(Button)findViewById(R.id.button);
        valider.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Activity2.class);
                //a la main
                intent.putExtra("nom",((EditText)findViewById(R.id.editText)).getText().toString());
                intent.putExtra("prenom",((EditText)findViewById(R.id.editText2)).getText().toString());
                intent.putExtra("date de naissance",((EditText)findViewById(R.id.editText3)).getText().toString());
                intent.putExtra("ville",((EditText)findViewById(R.id.editText4)).getText().toString());
                //avec un parcelable
                RecuperationData recup = new RecuperationData(((EditText)findViewById(R.id.editText)).getText().toString(),((EditText)findViewById(R.id.editText2)).getText().toString(),((EditText)findViewById(R.id.editText3)).getText().toString(),((EditText)findViewById(R.id.editText4)).getText().toString());
                intent.putExtra("recuperationData",recup);
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "nom : "+((EditText)findViewById(R.id.editText)).getText(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "prénom : "+((EditText)findViewById(R.id.editText2)).getText(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "date naissance : "+((EditText)findViewById(R.id.editText3)).getText(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "ville naissance : "+((EditText)findViewById(R.id.editText4)).getText(), Toast.LENGTH_SHORT).show();
            }
        });


    }
    //ajoute les ... menu en haut a droite
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.raz:
                raz();
                return true;
            case R.id.pn:
                phonenumber();
                return true;
            case R.id.wiki:
                wiki();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void raz(){
        ((EditText)findViewById(R.id.editText)).setText("", TextView.BufferType.EDITABLE);
        ((EditText)findViewById(R.id.editText2)).setText("", TextView.BufferType.EDITABLE);
        ((EditText)findViewById(R.id.editText3)).setText("", TextView.BufferType.EDITABLE);
        ((EditText)findViewById(R.id.editText4)).setText("", TextView.BufferType.EDITABLE);
    }

    private void phonenumber(){
        ViewGroup view = ((ViewGroup)findViewById(R.id.activity_main));
        EditText ed = new EditText(getApplicationContext());
//        ((RelativeLayout.LayoutParams)ed.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM,((EditText)findViewById(R.id.editText4)).getId());
//        ((RelativeLayout.LayoutParams)ed.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM,((EditText)findViewById(R.id.editText4)).getId());
//        ((RelativeLayout.LayoutParams)ed.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM,((EditText)findViewById(R.id.editText4)).getId());
//        android:layout_alignBottom="@+id/textView4"
//        android:layout_alignRight="@+id/editText3"
//        android:layout_alignEnd="@+id/editText3"
        view.addView(ed,8);
        TextView tv = new TextView(getApplicationContext());
        tv.setText("Phone number");
        view.addView(tv,8);
    }
    private void wiki(){
        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("http://fr.wikipedia.org/wiki/"+((EditText)findViewById(R.id.editText4)).getText()));
        startActivity(intent);
    }
}
