package com.example.yousra.mmmtp3;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;


public class MainActivity extends AppCompatActivity{

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       setContentView(R.layout.layout);

        if(findViewById(R.id.layoutsmall)!=null){
            Log.e("","En mode Small");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentLayout1,new ListFragmentForSmall())
                    .commit();
        }
        else
        if(findViewById(R.id.layoutlarge)!=null){
            Log.e("","En mode Large");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentLayout1,new ListFragment())
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentLayout2,new WebViewFragment())
                    .commit();
        }
       //if(findViewById(R.layout.layout.xml).getId() == "small")

       // setContentView(R.layout.Layout_Large);
       /* getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentLayout1,new ListFragment())
                .commit();
       getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentLayout2,new WebViewFragment())
                .commit();*/

    //    findViewById(R.layoutsmall.my_layout)
        /*getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentLayout3,new FragmentMap())
                .commit();
*/
    }


}
