package com.example.yousra.mmmtp3;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by yousra on 07/02/17.
 */

public class WebViewFragmentForSmall extends Fragment {





        WebView myWebView;
        String url;
        Button localiser;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            Bundle b=getArguments();
            if(b !=null)
                url = b.getString("url");

            return inflater.inflate(R.layout.fragment_web_view, container, false);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            myWebView = (WebView) view.findViewById(R.id.webview);
            if (url != null) {
                Toast.makeText(getContext(), url.replace("{region=", "").replace("}", "").toLowerCase(), Toast.LENGTH_SHORT).show();

                // if (url.replace("{region=", "").replace("}", "").toLowerCase() == "alsace") {
                myWebView.loadUrl("http://technoresto.org/vdf/" + url.replace("{region=", "").replace("}", "").toLowerCase() + "/index.html");
                //  Toast.makeText(getContext(), url.replace("{region=", "").replace("}", "").toLowerCase(), Toast.LENGTH_SHORT).show();
                // }
           /*  else {
            myWebView.loadUrl("http://technoresto.org/vdf/" + url.replace("{region=", "").replace("}", "").toLowerCase() + "/index.html");
            Toast.makeText(getContext(), url.replace("{region=", "").replace("}", "").toLowerCase(), Toast.LENGTH_SHORT).show();

        }*/

            }
            else
                myWebView.loadUrl("http://technoresto.org/vdf/index.html");

            localiser=(Button)view.findViewById(R.id.button);
            localiser.setOnClickListener( new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Bundle args= new Bundle();
                    Fragment f=new FragmentMap();
                    args.putString("region",url);
                    f.setArguments(args);
                    FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                    //FragmentTransaction permet de faire un back
                    FragmentTransaction fragmentT=fragmentManager.beginTransaction();
                    fragmentT.replace(R.id.fragmentLayout1,f);
                    fragmentT.addToBackStack(null);
                    fragmentT.commit();
                }
            });
        }
    }


