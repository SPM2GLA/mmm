package com.example.yousra.mmmtp3;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class ListFragment extends Fragment {

    private ListView malistView;
    private SimpleAdapter mlistAdapter;
    private ArrayList< HashMap<String, String>> mapItems;


    //On déclare la HashMap qui contiendra les informations pour un item
    HashMap<String, String> map;
    static final  int NEW_CLIENT=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       malistView=((ListView) view.findViewById(R.id.listview));
        //Création de la ArrayList qui nous permettra de remplire la listView
        mapItems = new ArrayList<HashMap<String, String>>();



//enfin on ajoute cette hashMap dans la arrayList


        ArrayList<String> mapItems2 = new ArrayList<String>();
        mapItems2.add("Alsace");
        mapItems2.add("Beaujolais");
        mapItems2.add("Jura");
        mapItems2.add("Champagne");
        mapItems2.add("Savoie");
        mapItems2.add("Bordelais");
        for (String elem : mapItems2){
            HashMap b = new HashMap<String, String>();
                    b.put("region",elem);
            mapItems.add(b);
        }

        //Création d'un SimpleAdapter qui se chargera de mettre les items présent dans notre list (mapItems) dans la vue item.xml
        mlistAdapter = new SimpleAdapter (this.getContext(), mapItems, R.layout.item,
                new String[] {"region"}, new int[] {R.id.textView1});
        //ici on affecte l'adapteur pour la listView afin de la remplir avec les elemets de item
        malistView.setAdapter(mlistAdapter);

        malistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {


                String message=malistView.getItemAtPosition(position).toString();
                 Bundle args= new Bundle();
                Fragment f=new WebViewFragment();
               args.putString("url",message);
                f.setArguments(args);

                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentT=fragmentManager.beginTransaction();
                fragmentT.replace(R.id.fragmentLayout2,f);
               fragmentT.addToBackStack(null);
                fragmentT.commit();
             //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();



            }
        });

    }






}