package com.example.yousra.mmmtp3;

import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.IOException;
import java.util.List;

public class FragmentMap extends Fragment {


    GoogleMap map;
    String region;
    Geocoder geocoder;
    List<Address> addresses;
    Marker marker;
    double latitude;
    double longitude;
    View v;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
           if(v==null)

            v=inflater.inflate(R.layout.activity_fragment_map, container, false);

       // map = ((SupportMapFragment) getFragmentManager()
          if(map==null)     // .findFragmentById(R.id.map)).getMap();
              // .findFragmentById(R.id.map)).getMap();
        map=((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        region = getArguments().getString("region").replace("{region=", "").replace("}", "").toLowerCase();
        geocoder = new Geocoder(getContext());

        return v;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        try {
            addresses = geocoder.getFromLocationName(region, 1);
            if (addresses.size() > 0) {
                latitude = addresses.get(0).getLatitude();
                longitude = addresses.get(0).getLongitude();
            }

            if (map != null)
                marker = map.addMarker(new MarkerOptions().position(new LatLng(latitude,longitude))
                        .title(region + ""));


            }catch (IOException e) {
            e.printStackTrace();
        }



        }







    }




