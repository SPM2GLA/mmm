package com.example.yousra.mmmtp1;

/**
 * Created by yousra on 18/01/17.
 */

        import android.content.ContentProvider;
        import android.content.ContentResolver;
        import android.content.ContentValues;
        import android.content.Context;
        import android.database.Cursor;
        import android.database.MatrixCursor;
        import android.database.SQLException;
        import android.database.sqlite.SQLiteDatabase;
        import android.net.Uri;
        import android.support.annotation.Nullable;

        import java.util.ArrayList;
        import java.util.List;


public class LibraryContentProvider extends ContentProvider {


    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    public LibraryContentProvider() {
    }

    public static Contact data[];

    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_LASTNAME, MySQLiteHelper.COLUMN_FIRSTNAME, MySQLiteHelper.COLUMN_BIRTHDAY, MySQLiteHelper.COLUMN_CITY };


    // This must be the same as what as specified as the Content Provider authority
    // in the manifest file.



    public LibraryContentProvider(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }
    public void close() {
        dbHelper.close();
    }


    @Override
    public boolean onCreate() {
        // initialiser des données ici (on simule l'existence d'une BD)

        //data = new Contact[2];
        //data[0] = new Contact("EL GHZIZAL", "Yousra","1951","FEZ");

        //data[1] = new Contact("Stephen","Piton","1988","ploemeur");

        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    public List<Contact> getAllContacts() {
        List<Contact> contacts = new ArrayList<Contact>();
        Cursor cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Contact contact = cursorToContact(cursor);
            contacts.add(contact);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();
        return contacts;
    }




    public void update(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_LASTNAME, contact.getName());
        values.put(MySQLiteHelper.COLUMN_FIRSTNAME, contact.getPrenom());
        values.put(MySQLiteHelper.COLUMN_BIRTHDAY, contact.getDateNaissance());
        values.put(MySQLiteHelper.COLUMN_CITY, contact.getVille());

        database.update(MySQLiteHelper.TABLE_CONTACT, values,
                MySQLiteHelper.COLUMN_ID + " = ? ",
                new String[] { String.valueOf(contact.getId()) });
    }



    public Contact insert(Contact c) {
        ContentValues values = new ContentValues();

        values.put(MySQLiteHelper.COLUMN_LASTNAME, c.getName());
        values.put(MySQLiteHelper.COLUMN_FIRSTNAME, c.getPrenom());
        values.put(MySQLiteHelper.COLUMN_BIRTHDAY, c.getDateNaissance());
        values.put(MySQLiteHelper.COLUMN_CITY, c.getVille());

        long insertId = database.insert(MySQLiteHelper.TABLE_CONTACT, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Contact newContact = cursorToContact(cursor);
        cursor.close();
        return newContact;
    }


    public void delete(Contact contact) {
        long id = contact.getId();
        database.delete(MySQLiteHelper.TABLE_CONTACT,
                MySQLiteHelper.COLUMN_ID + " = " + id, null);

    }

    private Contact cursorToContact(Cursor cursor) {
      Contact contact = new Contact();
        contact.setId(cursor.getLong(0));
        contact.setName(cursor.getString(1));
        contact.setPrenom(cursor.getString(2));
        contact.setDateNaissance(cursor.getString(3));
        contact.setVille(cursor.getString(4));
        return contact;
    }

}