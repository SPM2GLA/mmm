package com.example.yousra.mmmtp1;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import static java.sql.DriverManager.println;

public class MainActivity extends AppCompatActivity {
    LibraryContentProvider contact= new LibraryContentProvider(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button valider=(Button)findViewById(R.id.button);
        valider.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent=getIntent();//new Intent(MainActivity.this,Activity2.class);
                //SANS SQLITE PARTIE 1
//                intent.putExtra("nom",((EditText)findViewById(R.id.editText)).getText().toString());
//                intent.putExtra("prenom",((EditText)findViewById(R.id.editText2)).getText().toString());
//                intent.putExtra("dateNaissance",((EditText)findViewById(R.id.editText3)).getText().toString());
//                intent.putExtra("ville",((EditText)findViewById(R.id.editText4)).getText().toString());
                //AVEC SQLITE PARTIE 2
                contact.open();

                contact.insert(new Contact(((EditText)findViewById(R.id.editText)).getText().toString(),((EditText)findViewById(R.id.editText2)).getText().toString(),
                        ((EditText)findViewById(R.id.editText3)).getText().toString(),((EditText)findViewById(R.id.editText4)).getText().toString()));
                intent.putExtra("nom",contact.getAllContacts().get(((contact.getAllContacts().size()))-1).getName());
                intent.putExtra("prenom",contact.getAllContacts().get(((contact.getAllContacts().size()))-1).getPrenom());
                intent.putExtra("dateNaissance",contact.getAllContacts().get(((contact.getAllContacts().size()))-1).getDateNaissance());
                intent.putExtra("ville",contact.getAllContacts().get(((contact.getAllContacts().size()))-1).getVille());
                setResult(RESULT_OK,intent);
                finish();
                contact.close();

            }
        });



    }


    //ajoute les ... menu en haut a droite
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.raz:
                raz();
                return true;
            case R.id.pn:
                phonenumber();
                return true;
            case R.id.wiki:
                wiki();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void raz(){
        ((EditText)findViewById(R.id.editText)).setText("", TextView.BufferType.EDITABLE);
        ((EditText)findViewById(R.id.editText2)).setText("", TextView.BufferType.EDITABLE);
        ((EditText)findViewById(R.id.editText3)).setText("", TextView.BufferType.EDITABLE);
        ((EditText)findViewById(R.id.editText4)).setText("", TextView.BufferType.EDITABLE);
    }

    private void phonenumber(){
        ViewGroup view = ((ViewGroup)findViewById(R.id.activity_main));
        EditText ed = new EditText(getApplicationContext());
//        ((RelativeLayout.LayoutParams)ed.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM,((EditText)findViewById(R.id.editText4)).getId());
//        ((RelativeLayout.LayoutParams)ed.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM,((EditText)findViewById(R.id.editText4)).getId());
//        ((RelativeLayout.LayoutParams)ed.getLayoutParams()).addRule(RelativeLayout.ALIGN_BOTTOM,((EditText)findViewById(R.id.editText4)).getId());
//        android:layout_alignBottom="@+id/textView4"
//        android:layout_alignRight="@+id/editText3"
//        android:layout_alignEnd="@+id/editText3"
        view.addView(ed,8);
        TextView tv = new TextView(getApplicationContext());
        tv.setText("New Phone number ");
        view.addView(tv,8);
    }
    private void wiki(){
        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("http://fr.wikipedia.org/wiki/"+((EditText)findViewById(R.id.editText4)).getText()));
        startActivity(intent);
    }
}
