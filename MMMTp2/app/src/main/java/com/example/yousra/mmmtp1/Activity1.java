package com.example.yousra.mmmtp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Activity1 extends AppCompatActivity {
    private ListView malistView;
    private SimpleAdapter mlistAdapter;
   // private HashMap<String, String> map2;
    private ArrayList<HashMap<String, String>> mapItems;
    LibraryContentProvider contact= new LibraryContentProvider(this);

    //On déclare la HashMap qui contiendra les informations pour un item
    HashMap<String, String> map;
    static final  int NEW_CLIENT=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        EditText filter = (EditText) findViewById(R.id.filter);


        Button valider = (Button) findViewById(R.id.button2);
        valider.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Activity1.this, MainActivity.class);
                startActivityForResult(intent, NEW_CLIENT);
            }
        });


        malistView = ((ListView) findViewById(R.id.listview));
        //Création de la ArrayList qui nous permettra de remplire la listView
        mapItems = new ArrayList<>();
        map = new HashMap<String, String>();
        map.put("name", "elghzizal  ");
        map.put("prenom", "yousra  ");
        map.put("dateNaissance", "1994  ");
        map.put("ville", "fez");
//
////enfin on ajoute cette hashMap dans la arrayList
        mapItems.add(map);

        List<Contact> liste = new ArrayList<>();
        contact.open();
        liste = contact.getAllContacts();
        for (int i = 0; i < liste.size(); i++) {

            HashMap<String, String> map2=new HashMap<String, String>();
            map2.put("name", liste.get(i).getName()+"  ");
         map2.put("prenom", liste.get(i).getPrenom()+"  ");
            map2.put("dateNaissance", liste.get(i).getDateNaissance()+"  ");
            map2.put("ville", liste.get(i).getVille());
            mapItems.add(map2);
        }
        contact.close();

            // Création d'un SimpleAdapter qui se chargera de mettre les items présent dans notre list (mapItems) dans la vue item.xml
            mlistAdapter = new SimpleAdapter(this.getBaseContext(), mapItems, R.layout.item,
                    new String[]{"name", "prenom", "dateNaissance", "ville"}, new int[]{R.id.textView1, R.id.textView2, R.id.textView3, R.id.textView4});
            //ici on affecte l'adapteur pour la listView afin de la remplir avec les elemets de item
            malistView.setAdapter(mlistAdapter);
        //Fonction Pour la recherche par nom :
       TextWatcher inputfilter = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
             //  contact.open();
               //for(int i=0;i< contact.getAllContacts().size();i++) {
                 //  s = contact.getAllContacts().get(i).getName();
                   mlistAdapter.getFilter().filter(s);
               //}
                //contact.close();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }


        };

        filter.addTextChangedListener(inputfilter );

    }


    public void onActivityResult(int requestcode,int resultcode,Intent data){
        if(requestcode==NEW_CLIENT){
            if(resultcode==RESULT_OK) {
                map = new HashMap<String, String>();
                map.put("name", data.getStringExtra("nom"));
                map.put("prenom", data.getStringExtra("prenom"));
                map.put("dateNaissance", data.getStringExtra("dateNaissance"));
                map.put("ville", data.getStringExtra("ville"));
                mapItems.add(map);
                mlistAdapter.notifyDataSetChanged();

            }
        }
    }

}
